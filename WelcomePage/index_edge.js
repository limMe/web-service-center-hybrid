/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
var opts = {
    'gAudioPreloadPreference': 'auto',

    'gVideoPreloadPreference': 'auto'
};
var resources = [
];
var symbols = {
"stage": {
    version: "4.0.0",
    minimumCompatibleVersion: "4.0.0",
    build: "4.0.0.359",
    baseState: "Base State",
    scaleToFit: "width",
    centerStage: "both",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
            {
                id: 'pic1',
                type: 'group',
                rect: ['129', '150px','462','1009','auto', 'auto'],
                c: [
                {
                    id: '_1_backgroung2',
                    type: 'image',
                    rect: ['25px', '150px','411px','486px','auto', 'auto'],
                    fill: ["rgba(0,0,0,0)",'img/1_backgroung.png','0px','0px']
                },
                {
                    id: '_1_OneTable2',
                    type: 'image',
                    rect: ['104px', '0px','254px','350px','auto', 'auto'],
                    fill: ["rgba(0,0,0,0)",'img/1_OneTable.png','0px','0px']
                },
                {
                    id: 'title1_210_700',
                    type: 'text',
                    rect: ['81px', '493px','auto','auto','auto', 'auto'],
                    text: "一张表",
                    font: ['Arial, Helvetica, sans-serif', 100, "rgba(102,102,102,1.00)", "700", "none", ""]
                },
                {
                    id: 'content1_129_835_462',
                    type: 'text',
                    rect: ['0px', '810px','462px','199px','auto', 'auto'],
                    text: "您的基本信息都会保存在数据库的一张表内，被各项服务灵活调用，无需重复填写。",
                    align: "center",
                    font: ['Arial, Helvetica, sans-serif', 32, "rgba(102,102,102,1)", "400", "none", "normal"]
                }]
            },
            {
                id: 'title2',
                type: 'text',
                rect: ['545px', '700px','auto','auto','auto', 'auto'],
                text: "多样性",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 100, "rgba(102,102,102,1)", "bold", "none", "normal"]
            },
            {
                id: 'content2',
                type: 'text',
                rect: ['435px', '835px','522px','115px','auto', 'auto'],
                text: "填表的行为可以延伸为预约、报修、报销，甚至挂号、信访等服务。",
                align: "center",
                font: ['Arial, Helvetica, sans-serif', 32, "rgba(102,102,102,1)", "400", "none", "normal"]
            },
            {
                id: '_2_setting',
                type: 'image',
                rect: ['436px', '238px','131px','131px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/2_setting.png','0px','0px']
            },
            {
                id: '_2_culculator',
                type: 'image',
                rect: ['423px', '508px','117px','146px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/2_culculator.png','0px','0px']
            },
            {
                id: '_2_docment',
                type: 'image',
                rect: ['304px', '323px','178px','213px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/2_docment.png','0px','0px']
            },
            {
                id: '_2_clock',
                type: 'image',
                rect: ['209px', '485px','120px','120px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/2_clock.png','0px','0px']
            },
            {
                id: '_2_chat',
                type: 'image',
                rect: ['246px', '180px','127px','100px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/2_chat.png','0px','0px']
            },
            {
                id: '_2_chart',
                type: 'image',
                rect: ['148px', '291px','106px','106px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/2_chart.png','0px','0px']
            },
            {
                id: 'sun',
                type: 'rect',
                rect: ['473px', '221px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: '_3_whether',
                type: 'image',
                rect: ['137px', '213px','411px','208px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/3_whether.png','0px','0px']
            },
            {
                id: '_3_imac',
                type: 'image',
                rect: ['212px', '365px','297px','247px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/3_imac.png','0px','0px']
            },
            {
                id: '_3_ipad',
                type: 'image',
                rect: ['157px', '467px','110px','155px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/3_ipad.png','0px','0px']
            },
            {
                id: '_3_macbook',
                type: 'image',
                rect: ['360px', '485px','247px','142px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/3_macbook.png','0px','0px']
            },
            {
                id: '_3_iphone5',
                type: 'image',
                rect: ['113px', '536px','47px','99px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/3_iphone5.png','0px','0px']
            },
            {
                id: 'content3',
                type: 'text',
                rect: ['100px', '835px','533px','115px','auto', 'auto'],
                text: "24小时在线，随时办理，进度可见，不管在PC还是在平板电脑上，都可以快速获得。",
                align: "center",
                font: ['Verdana, Geneva, sans-serif', 32, "rgba(102,102,102,1)", "400", "none", "normal"]
            },
            {
                id: 'title3',
                type: 'text',
                rect: ['160px', '700px','auto','auto','auto', 'auto'],
                text: "随时随地",
                align: "center",
                font: ['Verdana, Geneva, sans-serif', 100, "rgba(102,102,102,1)", "800", "none", "normal"]
            },
            {
                id: '_4_loginbtn',
                type: 'image',
                rect: ['196px', '947px','332px','86px','auto', 'auto'],
                cursor: ['pointer'],
                fill: ["rgba(0,0,0,0)",'img/4_loginbtn.png','0px','0px']
            },
            {
                id: '_4_title',
                type: 'image',
                rect: ['206px', '731px','327px','127px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/4_title.png','0px','0px'],
                transform: [[],[],[],['1.25688','1.25688']]
            },
            {
                id: 'Ellipse',
                type: 'ellipse',
                rect: ['270px', '1050px','20px','20px','auto', 'auto'],
                borderRadius: ["50%", "50%", "50%", "50%"],
                fill: ["rgba(58,76,62,1.00)"],
                stroke: [0,"rgba(0,0,0,1)","none"]
            },
            {
                id: 'Ellipse2',
                type: 'ellipse',
                rect: ['320px', '1050px','20px','20px','auto', 'auto'],
                borderRadius: ["50%", "50%", "50%", "50%"],
                fill: ["rgba(192,192,192,1)"],
                stroke: [0,"rgba(0,0,0,1)","none"]
            },
            {
                id: 'Ellipse3',
                type: 'ellipse',
                rect: ['370px', '1050px','20px','20px','auto', 'auto'],
                borderRadius: ["50%", "50%", "50%", "50%"],
                fill: ["rgba(192,192,192,1)"],
                stroke: [0,"rgba(0,0,0,1)","none"]
            },
            {
                id: 'Ellipse4',
                type: 'ellipse',
                rect: ['420px', '1050px','20px','20px','auto', 'auto'],
                borderRadius: ["50%", "50%", "50%", "50%"],
                fill: ["rgba(192,192,192,1)"],
                stroke: [0,"rgba(0,0,0,1)","none"]
            },
            {
                id: 'cyc1',
                type: 'rect',
                rect: ['567', '296','auto','auto','auto', 'auto']
            },
            {
                id: '_4_building',
                type: 'image',
                rect: ['0', '0','640px','295px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/4_building.png','0px','0px']
            },
            {
                id: 'cyc2',
                type: 'rect',
                rect: ['911', '451','auto','auto','auto', 'auto']
            },
            {
                id: 'cyc3',
                type: 'rect',
                rect: ['411', '410','auto','auto','auto', 'auto']
            },
            {
                id: '_4_logo',
                type: 'image',
                rect: ['284px', '233px','192px','64px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",'img/4_logo.png','0px','0px']
            }],
            symbolInstances: [
            {
                id: 'cyc1',
                symbolName: 'cyc1',
                autoPlay: {

                }
            },
            {
                id: 'sun',
                symbolName: 'sun',
                autoPlay: {

                }
            },
            {
                id: 'cyc3',
                symbolName: 'cyc3',
                autoPlay: {

                }
            },
            {
                id: 'cyc2',
                symbolName: 'cyc2',
                autoPlay: {

                }
            }
            ]
        },
    states: {
        "Base State": {
            "${_Ellipse2}": [
                ["style", "height", '20px'],
                ["style", "top", '1050px'],
                ["style", "left", '320px'],
                ["style", "width", '20px']
            ],
            "${_Text2Copy}": [
                ["style", "top", '850px'],
                ["style", "width", '462px'],
                ["style", "font-weight", '400'],
                ["style", "opacity", '0'],
                ["style", "left", '0px'],
                ["style", "font-size", '32px']
            ],
            "${__3_imac}": [
                ["style", "top", '65px'],
                ["style", "opacity", '0'],
                ["style", "left", '212px']
            ],
            "${_title3}": [
                ["style", "top", '700px'],
                ["style", "opacity", '0'],
                ["style", "font-weight", '800'],
                ["style", "left", '460px'],
                ["style", "font-size", '100px']
            ],
            "${__2_culculator}": [
                ["style", "top", '708px'],
                ["style", "opacity", '0'],
                ["style", "left", '739px']
            ],
            "${_content2}": [
                ["style", "opacity", '0'],
                ["style", "left", '435px'],
                ["style", "font-size", '32px'],
                ["style", "top", '835px'],
                ["style", "text-align", 'center'],
                ["style", "font-weight", '400'],
                ["style", "height", '115px'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "width", '522px'],
                ["transform", "skewY", '0deg']
            ],
            "${_sun}": [
                ["style", "top", '221px'],
                ["style", "opacity", '1'],
                ["style", "left", '473px']
            ],
            "${_Ellipse3}": [
                ["style", "height", '20px'],
                ["style", "top", '1050px'],
                ["style", "left", '370px'],
                ["style", "width", '20px']
            ],
            "${__4_title}": [
                ["style", "top", '731px'],
                ["transform", "scaleY", '1.25688'],
                ["transform", "scaleX", '1.25688'],
                ["style", "opacity", '0'],
                ["style", "left", '848px']
            ],
            "${__3_iphone5}": [
                ["style", "top", '236px'],
                ["style", "opacity", '0'],
                ["style", "left", '113px']
            ],
            "${__2_setting}": [
                ["style", "top", '73px'],
                ["style", "opacity", '0'],
                ["style", "left", '655px']
            ],
            "${_Ellipse}": [
                ["style", "top", '1050px'],
                ["style", "height", '20px'],
                ["color", "background-color", 'rgba(58,76,62,1.00)'],
                ["style", "left", '270px'],
                ["style", "width", '20px']
            ],
            "${_content3}": [
                ["style", "top", '835px'],
                ["style", "opacity", '0'],
                ["style", "height", '115px'],
                ["style", "font-family", 'Verdana, Geneva, sans-serif'],
                ["style", "left", '400px'],
                ["style", "width", '533px']
            ],
            "${__3_ipad}": [
                ["style", "top", '167px'],
                ["style", "opacity", '0'],
                ["style", "left", '157px']
            ],
            "${__3_whether}": [
                ["style", "top", '213px'],
                ["style", "opacity", '0'],
                ["style", "left", '437px']
            ],
            "${__4_loginbtn}": [
                ["style", "top", '947px'],
                ["style", "opacity", '0'],
                ["style", "left", '838px'],
                ["style", "cursor", 'pointer']
            ],
            "${__2_docment}": [
                ["style", "top", '23px'],
                ["style", "opacity", '0'],
                ["style", "left", '304px']
            ],
            "${_Ellipse4}": [
                ["style", "height", '20px'],
                ["style", "top", '1050px'],
                ["style", "left", '420px'],
                ["style", "width", '20px']
            ],
            "${_TextCopy}": [
                ["style", "top", '550px'],
                ["style", "opacity", '0'],
                ["style", "font-weight", 'bold'],
                ["color", "color", 'rgba(102,102,102,1)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '-449px'],
                ["style", "font-size", '100px']
            ],
            "${__3_macbook}": [
                ["style", "top", '185px'],
                ["style", "opacity", '0'],
                ["style", "left", '360px']
            ],
            "${_title1_210_700}": [
                ["style", "top", '393px'],
                ["style", "opacity", '0.10000000149011612'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["color", "color", 'rgba(102,102,102,1.00)'],
                ["style", "font-weight", '700'],
                ["style", "left", '81px'],
                ["style", "font-size", '100px']
            ],
            "${__2_clock}": [
                ["style", "top", '689px'],
                ["style", "opacity", '0'],
                ["style", "left", '-125px']
            ],
            "${__1_backgroung2}": [
                ["style", "top", '230px'],
                ["transform", "scaleY", '1'],
                ["transform", "scaleX", '1'],
                ["style", "opacity", '0'],
                ["style", "left", '24px'],
                ["style", "-webkit-transform-origin", [50,50], {valueTemplate:'@@0@@% @@1@@%'} ],
                ["style", "-moz-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-ms-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "msTransformOrigin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-o-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}]
            ],
            "${_Stage}": [
                ["color", "background-color", 'rgba(255,255,255,1)'],
                ["style", "overflow", 'hidden'],
                ["style", "height", '1280px'],
                ["style", "max-width", '720px'],
                ["style", "width", '720px']
            ],
            "${__4_logo}": [
                ["style", "top", '213px'],
                ["style", "opacity", '0'],
                ["style", "left", '672px']
            ],
            "${__1_OneTable2}": [
                ["style", "top", '-100px'],
                ["transform", "scaleY", '1'],
                ["transform", "scaleX", '1'],
                ["style", "opacity", '0.10000000149011612'],
                ["style", "left", '104px']
            ],
            "${__2_chat}": [
                ["style", "top", '-7px'],
                ["style", "opacity", '0.011985844017094'],
                ["style", "left", '120px']
            ],
            "${_TextCopy2}": [
                ["style", "top", '393px'],
                ["style", "opacity", '0.10000000149011612'],
                ["style", "font-weight", '700'],
                ["color", "color", 'rgba(102,102,102,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '81px'],
                ["style", "font-size", '100px']
            ],
            "${__4_building}": [
                ["style", "top", '247px'],
                ["style", "opacity", '0'],
                ["style", "left", '417px']
            ],
            "${_content1_129_835_462}": [
                ["style", "top", '850px'],
                ["style", "text-align", 'center'],
                ["style", "font-size", '32px'],
                ["style", "opacity", '0'],
                ["style", "font-weight", '400'],
                ["style", "left", '0px'],
                ["style", "width", '462px']
            ],
            "${__2_chart}": [
                ["style", "top", '291px'],
                ["style", "opacity", '0'],
                ["style", "left", '-125px']
            ],
            "${_TextCopy4}": [
                ["style", "top", '393px'],
                ["style", "opacity", '0.10000000149011612'],
                ["style", "font-weight", '700'],
                ["color", "color", 'rgba(102,102,102,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '81px'],
                ["style", "font-size", '100px']
            ],
            "${_TextCopy3}": [
                ["style", "top", '393px'],
                ["style", "opacity", '0.10000000149011612'],
                ["style", "font-weight", '700'],
                ["color", "color", 'rgba(102,102,102,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '81px'],
                ["style", "font-size", '100px']
            ],
            "${_title2}": [
                ["style", "top", '700px'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '545px'],
                ["style", "opacity", '0']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 6657,
            autoPlay: true,
            labels: {
                "1": 686,
                "2": 1543,
                "3": 2486,
                "4": 3000
            },
            timeline: [
                { id: "eid22", tween: [ "style", "${_content1_129_835_462}", "top", '685px', { fromValue: '850px'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid164", tween: [ "style", "${__3_ipad}", "opacity", '1', { fromValue: '0'}], position: 1886, duration: 343, easing: "easeOutQuad" },
                { id: "eid316", tween: [ "style", "${__3_ipad}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid695", tween: [ "style", "${__4_building}", "top", '247px', { fromValue: '247px'}], position: 3000, duration: 0, easing: "easeOutQuad" },
                { id: "eid351", tween: [ "style", "${__4_title}", "top", '731px', { fromValue: '731px'}], position: 3000, duration: 0, easing: "easeOutQuad" },
                { id: "eid32", tween: [ "style", "${_title1_210_700}", "left", '-449px', { fromValue: '81px'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid29", tween: [ "style", "${_content1_129_835_462}", "left", '-529px', { fromValue: '0px'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid334", tween: [ "style", "${_sun}", "top", '221px', { fromValue: '221px'}], position: 2486, duration: 0, easing: "easeOutQuad" },
                { id: "eid330", tween: [ "style", "${__3_imac}", "left", '-104px', { fromValue: '212px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid337", tween: [ "style", "${_sun}", "left", '155px', { fromValue: '473px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid169", tween: [ "style", "${__3_imac}", "opacity", '1', { fromValue: '0'}], position: 1971, duration: 343, easing: "easeOutQuad" },
                { id: "eid322", tween: [ "style", "${__3_imac}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid360", tween: [ "style", "${__4_loginbtn}", "opacity", '1', { fromValue: '0'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid167", tween: [ "style", "${__3_whether}", "opacity", '1', { fromValue: '0'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid320", tween: [ "style", "${__3_whether}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid701", tween: [ "style", "${__4_building}", "left", '20px', { fromValue: '417px'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid182", tween: [ "style", "${_title3}", "opacity", '1', { fromValue: '0'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid315", tween: [ "style", "${_title3}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid76", tween: [ "style", "${__2_culculator}", "left", '423px', { fromValue: '739px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid120", tween: [ "style", "${__2_culculator}", "left", '123px', { fromValue: '423px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid697", tween: [ "style", "${__4_logo}", "top", '213px', { fromValue: '213px'}], position: 3000, duration: 0, easing: "easeOutQuad" },
                { id: "eid75", tween: [ "style", "${__2_clock}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid117", tween: [ "style", "${__2_clock}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid41", tween: [ "style", "${__1_backgroung2}", "left", '-190px', { fromValue: '24px'}], position: 686, duration: 171, easing: "easeOutQuad" },
                { id: "eid86", tween: [ "style", "${__1_backgroung2}", "left", '-276px', { fromValue: '-190px'}], position: 857, duration: 171, easing: "easeOutQuad" },
                { id: "eid185", tween: [ "style", "${_content3}", "left", '100px', { fromValue: '400px'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid327", tween: [ "style", "${_content3}", "left", '-216px', { fromValue: '100px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid184", tween: [ "style", "${_title3}", "left", '160px', { fromValue: '460px'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid323", tween: [ "style", "${_title3}", "left", '-156px', { fromValue: '160px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid353", tween: [ "style", "${__4_loginbtn}", "top", '947px', { fromValue: '947px'}], position: 3000, duration: 0, easing: "easeOutQuad" },
                { id: "eid10", tween: [ "style", "${__1_OneTable2}", "opacity", '1', { fromValue: '0.10000000149011612'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid38", tween: [ "style", "${__1_OneTable2}", "opacity", '0', { fromValue: '1'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid20", tween: [ "style", "${_title1_210_700}", "top", '550px', { fromValue: '393px'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid363", tween: [ "style", "${__4_loginbtn}", "left", '196px', { fromValue: '838px'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid110", tween: [ "style", "${_content2}", "left", '100px', { fromValue: '435px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid124", tween: [ "style", "${_content2}", "left", '-200px', { fromValue: '100px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid608", tween: [ "style", "${_Ellipse3}", "left", '370px', { fromValue: '370px'}], position: 686, duration: 0, easing: "easeOutQuad" },
                { id: "eid617", tween: [ "style", "${_Ellipse3}", "left", '320px', { fromValue: '370px'}], position: 2314, duration: 0, easing: "easeOutQuad" },
                { id: "eid707", tween: [ "style", "${__4_logo}", "opacity", '1', { fromValue: '0'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid172", tween: [ "style", "${__3_ipad}", "top", '467px', { fromValue: '167px'}], position: 1886, duration: 343, easing: "easeOutQuad" },
                { id: "eid326", tween: [ "style", "${__3_macbook}", "left", '44px', { fromValue: '360px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid68", tween: [ "style", "${__2_chat}", "top", '180px', { fromValue: '-7px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid72", tween: [ "style", "${__2_chart}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid112", tween: [ "style", "${__2_chart}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid706", tween: [ "style", "${__4_building}", "opacity", '1', { fromValue: '0'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid702", tween: [ "style", "${__4_logo}", "left", '275px', { fromValue: '672px'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid364", tween: [ "style", "${__4_title}", "left", '206px', { fromValue: '848px'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid174", tween: [ "style", "${__3_whether}", "left", '137px', { fromValue: '437px'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid328", tween: [ "style", "${__3_whether}", "left", '-179px', { fromValue: '137px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid612", tween: [ "style", "${_Ellipse}", "left", '270px', { fromValue: '270px'}], position: 686, duration: 0, easing: "easeOutQuad" },
                { id: "eid614", tween: [ "style", "${_Ellipse}", "left", '320px', { fromValue: '270px'}], position: 1371, duration: 0, easing: "easeOutQuad" },
                { id: "eid616", tween: [ "style", "${_Ellipse}", "left", '370px', { fromValue: '320px'}], position: 2314, duration: 0, easing: "easeOutQuad" },
                { id: "eid618", tween: [ "style", "${_Ellipse}", "left", '420px', { fromValue: '370px'}], position: 2829, duration: 0, easing: "easeOutQuad" },
                { id: "eid74", tween: [ "style", "${__2_clock}", "top", '485px', { fromValue: '689px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid165", tween: [ "style", "${__3_iphone5}", "opacity", '1', { fromValue: '0'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid321", tween: [ "style", "${__3_iphone5}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid37", tween: [ "style", "${__1_OneTable2}", "left", '-196px', { fromValue: '104px'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid173", tween: [ "style", "${__3_imac}", "top", '365px', { fromValue: '65px'}], position: 1971, duration: 514, easing: "easeOutBounce" },
                { id: "eid170", tween: [ "style", "${__3_iphone5}", "top", '536px', { fromValue: '236px'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid105", tween: [ "style", "${_title2}", "top", '700px', { fromValue: '700px'}], position: 857, duration: 0, easing: "easeOutQuad" },
                { id: "eid108", tween: [ "style", "${_title2}", "left", '210px', { fromValue: '545px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid126", tween: [ "style", "${_title2}", "left", '-90px', { fromValue: '210px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid369", tween: [ "style", "${_title2}", "left", '-78px', { fromValue: '-90px'}], position: 1886, duration: 1049, easing: "easeOutQuad" },
                { id: "eid168", tween: [ "style", "${__3_macbook}", "opacity", '1', { fromValue: '0'}], position: 1800, duration: 343, easing: "easeOutQuad" },
                { id: "eid318", tween: [ "style", "${__3_macbook}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid70", tween: [ "style", "${__2_chart}", "left", '148px', { fromValue: '-125px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid113", tween: [ "style", "${__2_chart}", "left", '-152px', { fromValue: '148px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid183", tween: [ "style", "${_content3}", "opacity", '1', { fromValue: '0'}], position: 1714, duration: 343, easing: "easeOutQuad" },
                { id: "eid319", tween: [ "style", "${_content3}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid67", tween: [ "style", "${__2_chat}", "left", '246px', { fromValue: '120px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid115", tween: [ "style", "${__2_chat}", "left", '-54px', { fromValue: '246px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid73", tween: [ "style", "${__2_clock}", "left", '209px', { fromValue: '-125px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid116", tween: [ "style", "${__2_clock}", "left", '-91px', { fromValue: '209px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid109", tween: [ "style", "${_title2}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid127", tween: [ "style", "${_title2}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid329", tween: [ "style", "${__3_iphone5}", "left", '-203px', { fromValue: '113px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid336", tween: [ "style", "${_sun}", "opacity", '0', { fromValue: '1'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid610", tween: [ "style", "${_Ellipse2}", "left", '320px', { fromValue: '320px'}], position: 686, duration: 0, easing: "easeOutQuad" },
                { id: "eid615", tween: [ "style", "${_Ellipse2}", "left", '270px', { fromValue: '320px'}], position: 1371, duration: 0, easing: "easeOutQuad" },
                { id: "eid179", tween: [ "style", "${_content3}", "top", '835px', { fromValue: '835px'}], position: 2057, duration: 0, easing: "easeOutQuad" },
                { id: "eid103", tween: [ "style", "${_content2}", "top", '835px', { fromValue: '835px'}], position: 857, duration: 0, easing: "easeOutQuad" },
                { id: "eid15", tween: [ "style", "${__1_backgroung2}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid42", tween: [ "style", "${__1_backgroung2}", "opacity", '0', { fromValue: '1'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid177", tween: [ "style", "${_title3}", "top", '700px', { fromValue: '700px'}], position: 2057, duration: 0, easing: "easeOutQuad" },
                { id: "eid111", tween: [ "style", "${_content2}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid125", tween: [ "style", "${_content2}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid155", tween: [ "style", "${__3_whether}", "top", '213px', { fromValue: '213px'}], position: 2057, duration: 0, easing: "easeOutQuad" },
                { id: "eid77", tween: [ "style", "${__2_culculator}", "top", '508px', { fromValue: '708px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid361", tween: [ "style", "${__4_title}", "opacity", '1', { fromValue: '0'}], position: 2657, duration: 343, easing: "easeOutQuad" },
                { id: "eid606", tween: [ "style", "${_Ellipse4}", "left", '420px', { fromValue: '420px'}], position: 686, duration: 0, easing: "easeOutQuad" },
                { id: "eid619", tween: [ "style", "${_Ellipse4}", "left", '370px', { fromValue: '420px'}], position: 2829, duration: 0, easing: "easeOutQuad" },
                { id: "eid171", tween: [ "style", "${__3_macbook}", "top", '485px', { fromValue: '185px'}], position: 1800, duration: 343, easing: "easeOutQuad" },
                { id: "eid7", tween: [ "style", "${__1_OneTable2}", "top", '110px', { fromValue: '-100px'}], position: 0, duration: 686, easing: "easeOutBounce" },
                { id: "eid34", tween: [ "style", "${__1_OneTable2}", "top", '-140px', { fromValue: '110px'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid79", tween: [ "style", "${__2_docment}", "top", '323px', { fromValue: '23px'}], position: 857, duration: 686, easing: "easeOutBounce" },
                { id: "eid69", tween: [ "style", "${__2_chat}", "opacity", '1', { fromValue: '0.011985844017094'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid114", tween: [ "style", "${__2_chat}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid14", tween: [ "style", "${__1_backgroung2}", "top", '20px', { fromValue: '230px'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid39", tween: [ "style", "${__1_backgroung2}", "top", '356px', { fromValue: '20px'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid21", tween: [ "style", "${_title1_210_700}", "opacity", '1', { fromValue: '0.10000000149011612'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid33", tween: [ "style", "${_title1_210_700}", "opacity", '0', { fromValue: '1'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid80", tween: [ "style", "${__2_docment}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid119", tween: [ "style", "${__2_docment}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid66", tween: [ "style", "${__2_setting}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid123", tween: [ "style", "${__2_setting}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid324", tween: [ "style", "${__3_ipad}", "left", '-159px', { fromValue: '157px'}], position: 2486, duration: 343, easing: "easeOutQuad" },
                { id: "eid23", tween: [ "style", "${_content1_129_835_462}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid30", tween: [ "style", "${_content1_129_835_462}", "opacity", '0', { fromValue: '1'}], position: 686, duration: 343, easing: "easeOutQuad" },
                { id: "eid65", tween: [ "style", "${__2_setting}", "left", '436px', { fromValue: '655px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid122", tween: [ "style", "${__2_setting}", "left", '136px', { fromValue: '436px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid64", tween: [ "style", "${__2_setting}", "top", '238px', { fromValue: '73px'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid118", tween: [ "style", "${__2_docment}", "left", '4px', { fromValue: '304px'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid78", tween: [ "style", "${__2_culculator}", "opacity", '1', { fromValue: '0'}], position: 857, duration: 343, easing: "easeOutQuad" },
                { id: "eid121", tween: [ "style", "${__2_culculator}", "opacity", '0', { fromValue: '1'}], position: 1543, duration: 343, easing: "easeOutQuad" },
                { id: "eid210", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_sun}', [] ], ""], position: 0 },
                { id: "eid720", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_cyc2}', [] ], ""], position: 0 },
                { id: "eid725", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_cyc1}', [] ], ""], position: 0 },
                { id: "eid715", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_cyc3}', [] ], ""], position: 0 },
                { id: "eid211", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_sun}', [] ], ""], position: 1714.2857142857 },
                { id: "eid716", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_cyc3}', [0] ], ""], position: 2647 },
                { id: "eid726", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_cyc1}', [0] ], ""], position: 2647 },
                { id: "eid721", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['stop', '${_cyc2}', [0] ], ""], position: 2647 },
                { id: "eid717", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_cyc3}', [] ], ""], position: 2657 },
                { id: "eid727", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_cyc1}', [] ], ""], position: 2657 },
                { id: "eid722", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_cyc2}', [] ], ""], position: 2657 }            ]
        }
    }
},
"sun": {
    version: "4.0.0",
    minimumCompatibleVersion: "4.0.0",
    build: "4.0.0.359",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: '_3_sun',
                    type: 'image',
                    rect: ['-300px', '0px', '205px', '205px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'img/3_sun.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${__3_sun}": [
                ["style", "top", '0px'],
                ["transform", "rotateZ", '0deg'],
                ["style", "opacity", '0'],
                ["style", "left", '0px'],
                ["style", "-webkit-transform-origin", [50,50], {valueTemplate:'@@0@@% @@1@@%'} ],
                ["style", "-moz-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-ms-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "msTransformOrigin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-o-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}]
            ],
            "${symbolSelector}": [
                ["style", "height", '205px'],
                ["style", "width", '205px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 4000,
            autoPlay: true,
            labels: {
                "beginToRotate": 1000
            },
            timeline: [
                { id: "eid175", tween: [ "style", "${__3_sun}", "left", '-300px', { fromValue: '0px'}], position: 0, duration: 1000, easing: "easeOutQuad" },
                { id: "eid662", tween: [ "transform", "${__3_sun}", "rotateZ", '360deg', { fromValue: '0deg'}], position: 1000, duration: 3000 },
                { id: "eid166", tween: [ "style", "${__3_sun}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 1000, easing: "easeOutQuad" }            ]
        }
    }
},
"cyc3": {
    version: "4.0.0",
    minimumCompatibleVersion: "4.0.0",
    build: "4.0.0.359",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: '_4_cyc3',
                    type: 'image',
                    rect: ['-411px', '-410px', '39px', '40px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'img/4_cyc3.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${__4_cyc3}": [
                ["style", "top", '0px'],
                ["style", "opacity", '0'],
                ["style", "left", '397px'],
                ["transform", "rotateZ", '0deg']
            ],
            "${symbolSelector}": [
                ["style", "height", '40px'],
                ["style", "width", '39px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 1343,
            autoPlay: true,
            labels: {
                "beginToRotate": 343
            },
            timeline: [
                { id: "eid698", tween: [ "style", "${__4_cyc3}", "left", '0px', { fromValue: '397px'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid709", tween: [ "transform", "${__4_cyc3}", "rotateZ", '360deg', { fromValue: '0deg'}], position: 343, duration: 1000 },
                { id: "eid689", tween: [ "style", "${__4_cyc3}", "top", '0px', { fromValue: '0px'}], position: 343, duration: 0, easing: "easeOutQuad" },
                { id: "eid703", tween: [ "style", "${__4_cyc3}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 343, easing: "easeOutQuad" }            ]
        }
    }
},
"cyc2": {
    version: "4.0.0",
    minimumCompatibleVersion: "4.0.0",
    build: "4.0.0.359",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: '_4_cyc2',
                    type: 'image',
                    rect: ['-911px', '-451px', '91px', '91px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'img/4_cyc2.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${__4_cyc2}": [
                ["style", "top", '0px'],
                ["style", "opacity", '0'],
                ["style", "left", '0px'],
                ["transform", "rotateZ", '0deg']
            ],
            "${symbolSelector}": [
                ["style", "height", '91px'],
                ["style", "width", '91px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 3000,
            autoPlay: true,
            labels: {
                "beginToRotate": 343
            },
            timeline: [
                { id: "eid705", tween: [ "style", "${__4_cyc2}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid693", tween: [ "style", "${__4_cyc2}", "top", '0px', { fromValue: '0px'}], position: 343, duration: 0, easing: "easeOutQuad" },
                { id: "eid700", tween: [ "style", "${__4_cyc2}", "left", '-397px', { fromValue: '0px'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid719", tween: [ "transform", "${__4_cyc2}", "rotateZ", '360deg', { fromValue: '0deg'}], position: 343, duration: 2657 }            ]
        }
    }
},
"cyc1": {
    version: "4.0.0",
    minimumCompatibleVersion: "4.0.0",
    build: "4.0.0.359",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: '_4_cyc1',
                    type: 'image',
                    rect: ['-567px', '-296px', '151px', '151px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'img/4_cyc1.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${__4_cyc1}": [
                ["style", "top", '0px'],
                ["style", "opacity", '0'],
                ["style", "left", '0px'],
                ["transform", "rotateZ", '0deg']
            ],
            "${symbolSelector}": [
                ["style", "height", '151px'],
                ["style", "width", '151px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 4000,
            autoPlay: true,
            labels: {
                "beginToRotate": 343
            },
            timeline: [
                { id: "eid704", tween: [ "style", "${__4_cyc1}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 343, easing: "easeOutQuad" },
                { id: "eid724", tween: [ "transform", "${__4_cyc1}", "rotateZ", '360deg', { fromValue: '0deg'}], position: 343, duration: 3657 },
                { id: "eid691", tween: [ "style", "${__4_cyc1}", "top", '0px', { fromValue: '0px'}], position: 343, duration: 0, easing: "easeOutQuad" },
                { id: "eid699", tween: [ "style", "${__4_cyc1}", "left", '-397px', { fromValue: '0px'}], position: 0, duration: 343, easing: "easeOutQuad" }            ]
        }
    }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources, opts);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-8913753");
