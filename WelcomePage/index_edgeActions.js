/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      

      Symbol.bindElementAction(compId, symbolName, "${_Stage}", "swiperight", function(sym, e) {
         // insert code to be run when a swiperight event occurs on an element
         // Play the timeline backwards from a label or specific time. For example:
         // sym.playReverse(500); or sym.playReverse("myLabel");
         var flag = sym.getVariable("count");
         
         if(flag == 3)
         {
         	sym.getSymbol("sun").playReverse("beginToRotate");
         	sym.setVariable("isBack",1);
         }
         
         if(flag == 4)
         {
         	sym.getSymbol("cyc1").playReverse("beginToRotate");
         	sym.getSymbol("cyc2").playReverse("beginToRotate");
         	sym.getSymbol("cyc3").playReverse("beginToRotate");
         }
         
         if(flag != 1)
         {
         	sym.playReverse(flag.toString());
         }
         
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_Stage}", "swipeleft", function(sym, e) {
         // insert code to be run when a swipeleft event occurs on an element
         sym.play();
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 686, function(sym, e) {
         // insert code here
         sym.stop();
         sym.setVariable("count",1);
         
         sym.setVariable("order",1);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1543, function(sym, e) {
         // insert code here
         sym.stop();
         sym.setVariable("count",2);

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2486, function(sym, e) {
         // insert code here
         sym.stop();
         sym.setVariable("count",3);
         
         var order = sym.getVariable("order");
         
         if(order == 1)
         {
         	sym.setVariable("order",0);
         }
         if(order == 0)
         {
         	sym.setVariable("order",1);
         }

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         // insert code here
         sym.stop();
         sym.setVariable("count",4);

      });
      //Edge binding end

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1711, function(sym, e) {
         var isBack = sym.getVariable("isBack");
         if(isBack == 1)
         {
         	sym.getSymbol("sun").stop(0);
         }

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${__4_loginbtn}", "click", function(sym, e) {
         // insert code for mouse click here
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute)
         window.open("index.html", "_self");
         

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'sun'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         // insert code here
         // Play the timeline at a label or specific time. For example:
         // sym.play(500); or sym.play("myLabel");
         sym.play("beginToRotate");

      });
      //Edge binding end

   })("sun");
   //Edge symbol end:'sun'

   //=========================================================
   
   //Edge symbol: 'cyc3'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1343, function(sym, e) {
         // insert code here
         // Play the timeline at a label or specific time. For example:
         // sym.play(500); or sym.play("myLabel");
         sym.play("beginToRotate");

      });
      //Edge binding end

   })("cyc3");
   //Edge symbol end:'cyc3'

   //=========================================================
   
   //Edge symbol: 'cyc2'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         // insert code here
         // Play the timeline at a label or specific time. For example:
         // sym.play(500); or sym.play("myLabel");
         sym.play("beginToRotate");

      });
      //Edge binding end

   })("cyc2");
   //Edge symbol end:'cyc2'

   //=========================================================
   
   //Edge symbol: 'cyc1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         // insert code here
         // Play the timeline at a label or specific time. For example:
         // sym.play(500); or sym.play("myLabel");
         sym.play("beginToRotate");

      });
      //Edge binding end

   })("cyc1");
   //Edge symbol end:'cyc1'

})(jQuery, AdobeEdge, "EDGE-8913753");