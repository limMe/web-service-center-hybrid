function getChartList() {
    moveChildren("ChartPage");
    
    var url =domain+ "/servcenter/Admin/Index/getlist";
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",url);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			
			var chartlistJSON = JSON.parse(xmlhttp.responseText);
            
            if(chartlistJSON.code == 0)
            {
                
                for(var i=0;i<chartlistJSON.data.data.length;i++)
                {
                    addChartItem(chartlistJSON.data.data[i].aid,chartlistJSON.data.data[i].sname);
                }
                
            }
			
		}
	}
	xmlhttp.send();
}

function addChartItem(aid,sname) {
    
    if(sname.length > getProperLength(90,16))
    {
        sname=sname.substr(0,getProperLength(90,16))+"..";
    }
    
    var jsSentence = "javascript: initialLineChart('"+aid+"','"+sname+"')";
	var a = document.createElement("a");
	a.setAttribute("onClick",jsSentence);
	a.innerHTML = '<div><p>'+sname+'</p><img src="img/VectorRight.png" /></div>';	
	document.getElementById("ChartPage").appendChild(a);
    
}

function initialLineChart(aid,sname) {
    document.getElementById("footer").style.display = "none";
    
    //Prepare works
    moveChildren("change_screen_ul");
    var li = document.createElement("li");
    li.innerHTML = '<canvas id="lineChart" height="300px" width="300px">';

    document.getElementById("change_screen_ul").appendChild(li);
    if(sname.length > getProperLength(120,20))
    {
        sname=sname.substr(0,getProperLength(120,20))+"..";
    }
    showPanel(sname,"ChartDisplayPage");
    
    //Data requiring
    /*
    var url =domain+ "servcenter/Analysis/SingleServ/getSingleServ/aid/"+aid;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",url);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			
			var chartJSON = JSON.parse(xmlhttp.responseText);
            
            var lineDate = new Array();
            var lineNum = new Array();
            var lineData = {
                labels : ["测试","测试"],
                datasets : [
                {
			         fillColor : "rgba(151,187,205,0.5)",
			         strokeColor : "rgba(151,187,205,1)",
			         pointColor : "rgba(151,187,205,1)",
			         pointStrokeColor : "#fff",
			         data : [100,200], 
                }   
                ]
            }
            
            var pieData = new Array();
            
            
            for(var i=0;i<chartJSON.ctime.length;i++)
            {
                lineDate.push(chartJSON.ctime[i].ctime);
                lineNum.push(chartJSON.ctime[i].total);
            }
            lineData.labels = lineDate;
            lineData.datasets[0].data = lineNum;
            
            
            lineData.labels = new Array([1],[2],[4],[5]);
            lineData.datasets[0].data = new Array([1],[2],[4],[5]);
            
            //模拟枚举类型，用来配色
            var colorType =new Array(["#F7464A"],["#46BFBD"],["#FDB45C"],["#949FB1"],["#4D5360"]);
            var highlightType = new Array(["#FF5A5E"],["#5AD3D1"],["#FFC870"],["#A8B3C5"],["#616774"]);
            for(var i=0;i<chartJSON.status.length;i++)
            {
                var newPie = new Object();
                newPie.label=chartJSON.status[i].statusname;
                newPie.value=chartJSON.status[i].total;
                newPie.color=colorType[i];
                newPie.hightlight=highlightType[i];
                pieData.push(newPie);
            }
            
            
            var ctx = document.getElementById("lineChart").getContext("2d");
            new Chart(ctx).Line(lineData,{animation:true,
                              tooltipTemplate: "<%if (label){%><%=label%>提交了<%}%><%= value %>单",
                });
            new Chart(ctx).Pie(pieData,{animation:true,
                              tooltipTemplate: "<%if (label){%><%=label%>的总共有<%}%><%= value %>单",
                });
            document.getElementById("lineChart").style.height = "100%";
            
            document.getElementById("lineChart").style.paddingTop = "0px";
    
            document.getElementById("lineChart").style.width = "100%";
		}
	}
	xmlhttp.send();*/
    
    var pieData = [
	{
		value: 300,
		color:"#F7464A",
		highlight: "#FF5A5E",
		label: "签批中"
	},
	{
		value: 100,
		color: "#46BFBD",
		highlight: "#5AD3D1",
		label: "待受理"
	},
	{
		value: 100,
		color: "#FDB45C",
		highlight: "#FFC870",
		label: "已通过"
	}
    ]
    var ctx = document.getElementById("lineChart").getContext("2d");
    new Chart(ctx).Pie(pieData,{animation:true,
                              tooltipTemplate: "<%if (label){%><%=label%>的总共有<%}%><%= value %>单",
                });
}

