// JavaScript Document

function list_refresh() {
	
	document.getElementById("LoadingPage").style.display = "block";
	document.getElementById("undoneWords").style.visibility = "hidden";
	document.getElementById("doneWords").style.visibility = "hidden";
	document.getElementById("refreshIcon").src = "img/RefreshIcon.gif";
	
	del_old();
	
	var url = domain + "/servcenter/Index/getNeedToDone";
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",url);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){	
			var listJSON = JSON.parse(xmlhttp.responseText);
			
			document.getElementById("undoneWords").style.visibility = "visible";
			document.getElementById("doneWords").style.visibility = "visible";
			
			var i=0;
			if( AIsAuditPanel == 1)
			{
				for(i=0;i<listJSON.needaudit.length;i++)
				{
					list_addUncheck(listJSON.needaudit[i].aid,listJSON.needaudit[i].rid,listJSON.needaudit[i].sname,listJSON.needaudit[i].catname,listJSON.needaudit[i].uname,listJSON.needaudit[i].ctime,"本科生",listJSON.needaudit[i].uid,1);	
				}
			}
			if( AIsAuditPanel == 2)
			{
				for(i=0;i<listJSON.needcheck.length;i++)
				{
					list_addUncheck(listJSON.needcheck[i].aid,listJSON.needcheck[i].rid,listJSON.needcheck[i].sname,listJSON.needcheck[i].catname,listJSON.needcheck[i].uname,listJSON.needcheck[i].ctime,"本科生",listJSON.needcheck[i].uid,2);
				}
			}
			
			document.getElementById("LoadingPage").style.display = "none";
			
		}
	}
	xmlhttp.send();
	
	//已签批的部分
	
	var url2 = domain + "/servcenter/MyServ/auditServ?status=1";
	var xmlhttp2 = new XMLHttpRequest();
	xmlhttp2.open("GET",url2);
	xmlhttp2.onreadystatechange = function(){
		if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200){	
			var listJSON = JSON.parse(xmlhttp2.responseText);
			var i=0;
			for(i=0;i<listJSON.data.data.length && i<10;i++)
			{
				list_addChecked(listJSON.data.data[i].aid,listJSON.data.data[i].rid,listJSON.data.data[i].sname,listJSON.data.data[i].catname,listJSON.data.data[i].uname,listJSON.data.data[i].ctime,"本科生",listJSON.data.data[i].uid,1);	
			}
			document.getElementById("refreshIcon").src = "img/RefreshIcon.png";
		}
	}
	xmlhttp2.send();
	
	
}

function del_old() {
	//Delete the old items, maybe be too complex.
	var oldItems = document.getElementsByClassName("uncheckItem");
	for(var j=oldItems.length-1;j>=0;j--)
	{
		var a = oldItems[j];
		a.parentNode.removeChild(a);
	}
	oldItems = document.getElementsByClassName("checkedItem");
	for(var j=oldItems.length-1;j>=0;j--)
	{
		var a = oldItems[j];
		a.parentNode.removeChild(a);
	}
}

function auditPanelChange(isAudit) {
	document.getElementById("isAudit").style.display = "block";
	document.getElementById("panelName").style.display = "none";
	if(isAudit == 1)
	{
		document.getElementById("doneWords").innerHTML = "已签批";
		document.getElementById("undoneWords").innerHTML = "未签批";
		document.getElementById("isAudit").firstChild.firstChild.style.color = "#3a4c3e";
		document.getElementById("isAudit").firstChild.firstChild.style.backgroundColor = "#fff";
		document.getElementById("isAudit").lastChild.firstChild.style.color = "#fff";
		document.getElementById("isAudit").lastChild.firstChild.style.backgroundColor = "#3a4c3e";
	}
	if(isAudit == 2)
	{
		document.getElementById("doneWords").innerHTML = "已审核";
		document.getElementById("undoneWords").innerHTML = "未审核";
		document.getElementById("isAudit").firstChild.firstChild.style.color = "#fff";
		document.getElementById("isAudit").firstChild.firstChild.style.backgroundColor = "#3a4c3e";
		document.getElementById("isAudit").lastChild.firstChild.style.color = "#3a4c3e";
		document.getElementById("isAudit").lastChild.firstChild.style.backgroundColor = "#fff";
	}
}

function auditPanelSwitch(isAudit)
{
	AIsAuditPanel = isAudit;
	auditPanelChange(AIsAuditPanel);
	list_refresh();
}

function list_addUncheck(aid,rid,sname,type,uname,ctime,usertype,uid,isAudit) {
	if(getProperLength(120,16) < sname.length)
		sname = sname.substr(0,getProperLength(120,16))+"..";
	
	var jsSentence = "'"+aid+"','"+rid+"','"+isAudit+"'";//很多函数都用的这两个参数。
	var div = document.createElement("div");
	div.setAttribute("class","uncheckItem");
	div.innerHTML = '<a href="#" onClick="javascript:showDetail('+jsSentence+')"><div><img src="'+getUserPic(uid)+'" /><p class="itemTitle">'+sname+'</p><p class="itemDetail">类型：'+type+' 时间：'+ctime+'</p><p class="itemDetail">姓名：'+uname+'</p></div></a><div><div><a href="#" onClick="javascript:fastAudit('+jsSentence+',2)"><p>通过</p></a></div><a onClick="javascript:hideTwoBtns(this);"><img src="img/VectorUp.png" style="height:12px;width:12px;float:right;padding:24px 18px 18px 18px;" /></a><div><a href="#" onClick="javascript:fastAudit('+jsSentence+'0)"><p>驳回</p></a></div></div><a onClick="javascript:showTwoBtns(this);"><div class="coverBtns"><img src="img/VectorDown.png" /></div></a>';	
	document.getElementById("uncheck_start").appendChild(div);
}

function list_addChecked(aid,rid,sname,catname,uname,ctime,usertype,uid,isAudit) {
	if(getProperLength(120,16) < sname.length)
		sname = sname.substr(0,getProperLength(120,16))+"..";
		
	var jsSentence = "'"+aid+"','"+rid+"','"+isAudit+"'";//很多函数都用的这两个参数。
	var div = document.createElement("div");
	div.setAttribute("class","checkedItem");
	div.innerHTML = '<a href="#" onClick="javascript:showCheckedDetail('+jsSentence+')"><div><img src="'+getUserPic(uid)+'" /><p class="itemTitle">'+sname+'</p><p class="itemDetail">类型：' + catname +' 时间:' + ctime + '</p><p class="itemDetail">姓名：' + uname + '</p></div></a><div style="background-color:#f9f9f9;height:30px;padding:padding:12px 10px;"></div>';
	document.getElementById("checked_start").appendChild(div);
}

//待改进
//使用了同步的方式获得userpic，或许需要改进
function getUserPic(uid)
{
	var url = domain + "CrossDomainData/Notice/getUserInfo?uid=" + uid;
	var xmlhttp = new XMLHttpRequest();
	var what;
	xmlhttp.open("GET",url,false);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			var settingJSON = JSON.parse(xmlhttp.responseText);
			var reg=new RegExp("\/","g");
			settingJSON.data.logo50 = settingJSON.data.logo50.replace(reg,"/");
			what = settingJSON.data.logo50;
		}
	}
	xmlhttp.send();
	return what;
}

function showCheckedDetail(aid,rid,isAudit) 
{
	showDetail(aid,rid,isAudit);
	document.getElementById("footer2").style.display = "none";
}

function showDetail(aid,rid,isAudit) {
	document.getElementById("LoadingPage").style.display = "block";
	
	DFromDetail = 1;
	document.getElementById("backToHome").style.display = "block";
	document.getElementById("isAudit").style.display = "none";
	document.getElementById("refreshBtn").style.display = "none";
	document.getElementById("panelName").style.display = "block";
	document.getElementById("panelName").style.fontSize = "20px";
	var l=document.getElementsByClassName("panel").length;
	for(var i=0;i<l;i++)
	{
		document.getElementsByClassName("panel").item(i).style.display="none";
	}
	document.getElementById("DetailPage").style.display="block";
	document.getElementById("footer2").style.display="";
	
	var url = domain + "servcenter/Info/Table/record?aid="+aid+"&rid="+rid;
	//alert(url);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",url);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			var detailJSON = JSON.parse(xmlhttp.responseText);
			document.getElementById("Detail_SponsorName").innerHTML = detailJSON.data.user.real_name;
			if(detailJSON.data.sname.length > getProperLength(120,20))
			{
				document.getElementById("panelName").innerHTML = detailJSON.data.sname.substr(0,getProperLength(120,20)) + "..";
			}
			else
				document.getElementById("panelName").innerHTML = detailJSON.data.sname;
			
			
			var detail_start = document.getElementById("detail_start");
			for(;detail_start.hasChildNodes();)
			{
				detail_start.removeChild(detail_start.firstChild);
			}
			
			/*待改进：或许部分情况还没有弄完整*/
			for(var count=0; count < detailJSON.data.fieldInfo.length; count++)
			{
				//单行和多行文本的情况
				if(detailJSON.data.fieldInfo[count].t == 11 || detailJSON.data.fieldInfo[count].t == 12)
				{
					var pTitle = document.createElement("p");
					var pDetail = document.createElement("p");
					pTitle.setAttribute("class","detailTitle");
					pDetail.setAttribute("class","deatilContent");
					pTitle.innerHTML = detailJSON.data.fieldInfo[count].l;
					pDetail.innerHTML = detailJSON.data.fieldInfo[count].value;
					detail_start.appendChild(pTitle);
					detail_start.appendChild(pDetail);
					continue;
				}
				
				//分组
				if(detailJSON.data.fieldInfo[count].t == 13)
				{
					var pGroup = document.createElement("p");
					pGroup.setAttribute("class","detailGroup");
					pGroup.innerHTML= detailJSON.data.fieldInfo[count].l;
					var pLine = document.createElement("div");
					pLine.setAttribute("style","border-bottom:solid");
					detail_start.appendChild(pGroup);
					detail_start.appendChild(pLine);
					continue;
				}
				
				//pictures
				if(detailJSON.data.fieldInfo[count].t == 31)
				{
					var pTitle = document.createElement("p");
					pTitle.setAttribute("class","detailTitle");
					pTitle.innerHTML = detailJSON.data.fieldInfo[count].l;
					detail_start.appendChild(pTitle);
					if(detailJSON.data.fieldInfo[count].url != null)
					{
						var pImg = document.createElement("img");
						pImg.src = detailJSON.data.fieldInfo[count].url;
						detail_start.appendChild(pImg);
					}
					else
					{
						var pDetail = document.createElement("p");
						pDetail.setAttribute("class","deatilContent");
						pDetail.innerHTML = "无图片";
						detail_start.appendChild(pDetail);
					}
					continue;
				}
				
				//files
				if(detailJSON.data.fieldInfo[count].t == 32)
				{
					var pTitle = document.createElement("p");
                    var pDiv = document.createElement("div");
					pTitle.setAttribute("class","detailTitle");
					pTitle.innerHTML = detailJSON.data.fieldInfo[count].l;
					detail_start.appendChild(pTitle);
					if(detailJSON.data.fieldInfo[count].url != null)
					{
						pDiv.innerHTML = '<a onClick="javascript:downloadFile(\''+detailJSON.data.fieldInfo[count].url+'\')"><p class="detailContent" style="font-size:14px;">'+detailJSON.data.fieldInfo[count].oriname+'</p></a>';
                        pDiv.style.border = "solid";
                        pDiv.style.borderWidth = "thin";
                        pDiv.style.borderRadius = "5px";
                        pDiv.style.padding = "5px";
					}
					else
					{
						pDiv.innerHTML = '<p class="detailContent">没有上传文件</p>';
					}
					detail_start.appendChild(pDiv);
					continue;
				}
				
				//selections，多选的部分尚未通过测试
				if(detailJSON.data.fieldInfo[count].t == 21 || detailJSON.data.fieldInfo[count].t == 22)
				{
					var pTitle = document.createElement("p");
					var pDetail = document.createElement("p");
					pTitle.setAttribute("class","detailTitle");
					pDetail.setAttribute("class","deatilContent");
					pTitle.innerHTML = detailJSON.data.fieldInfo[count].l;
					var selections=detailJSON.data.fieldInfo[count].value.split(",");
					var selection;
					for(var i=0; i<selections.length; i++)
					{
						selection = parseInt(selections[i])-1;
						pDetail.innerHTML = pDetail.innerHTML + detailJSON.data.fieldInfo[count].c.is[selection].l +",";
					}
					detail_start.appendChild(pTitle);
					detail_start.appendChild(pDetail);
					continue;
				}
			}
			
			document.getElementById("LoadingPage").style.display = "none";
			
			var jsSentence = "'"+detailJSON.data.aid+"','"+detailJSON.data.rid+"','"+isAudit+"'";
			document.getElementById("rejectBtn").setAttribute("onClick","javascript: rejectItem(" + jsSentence +")");
			document.getElementById("passBtn").setAttribute("onClick","javascript: passItem(" + jsSentence +")");
		}
		
	}
	xmlhttp.send();
	document.getElementById("footer").style.display = "none";	
	document.getElementById("footer2").style.display = "block";
}

function passItem(aid,rid,isAudit)
{
	document.getElementById("backToHome").style.display = "none";
	showPanel("通过","AuditPage");
	Arid=rid;
	Aaid=aid;
	AIsPass=1;
	AIsAudit=isAudit;
	document.getElementById("footer").style.display = "none";	
	document.getElementById("footer2").style.display = "none";
}

function rejectItem(aid,rid,isAudit)
{
	document.getElementById("backToHome").style.display = "none";
	showPanel("驳回","AuditPage");
	Arid=rid;
	Aaid=aid;
	AIsPass=2;
	AIsAudit=isAudit;
	document.getElementById("footer").style.display = "none";	
	document.getElementById("footer2").style.display = "none";
}

function backToDetail()
{
	if(DFromDetail == 1)
	{
		showDetail(Aaid,Arid);
		document.getElementById("footer2").style.display = "block";
	}
	else
	{
		backToHome();
	}
}

function passAudit()
{
	var memo="";//默认签批意见
	
	try{
	memo = document.getElementById("AuditComment").innerHTML;
	}
	catch(e)
	{}
	
	var auditurl = domain+ "/servcenter/Info/Table/audit?";
	var checkurl = domain+ "/servcenter/Info/Table/check?";
	var xmlhttp = new XMLHttpRequest();
	if(AIsAudit == 1)
		xmlhttp.open("POST",auditurl);
	if(AIsAudit == 2)
		xmlhttp.open("POST",checkurl);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			var auditJSON = JSON.parse(xmlhttp.responseText);
			//Go back to home if success
			if(auditJSON.code == 0)
			{
				alert("签审成功！");
			}
			else
			{
				alert("可能由于网络或者用户原因，签审失败！");
			}
			backToHome();
			list_refresh();
		}
	}
	xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8") 
	if(AIsAudit == 1)
	{
		//alert("aid="+Aaid+"&rid="+Arid+"&audit="+AIsPass.toString()+"&memo="+memo);
		xmlhttp.send("aid="+Aaid+"&rid="+Arid+"&audit="+AIsPass.toString()+"&memo="+memo);
	}
	if(AIsAudit == 2)
		xmlhttp.send("aid="+Aaid+"&rid="+Arid+"&result="+AIsPass.toString());
		
	//恢复了默认的全局数据。
	Arid="";
	Aaid="";
	AIsAudit=0;
	AIsPass=0;
}

function auditKeyDown(e) 
{
	var currKey=0,e=e||event; 
	currKey=e.keyCode||e.which||e.charCode;
	if (currKey == 13) 
	{
		passAudit();
	}
} 


function showTwoBtns(obj)
{
	var initialHeight = 30;
	//obj.firstChild.innerHTML = "";
		var countTime = setInterval(function() { 
		initialHeight = initialHeight +2;
		obj.firstChild.style.height = initialHeight.toString()+"px" ;
		if(initialHeight == 50)
		{
			clearInterval(countTime);
			obj.style.display="none";
			obj.parentNode.childNodes[1].style.display = "block";
		}
		}, 50); 
	
	return false;
}

function hideTwoBtns(obj)
{
    obj.parentNode.style.display="none";
	obj.parentNode.parentNode.childNodes[2].style.display = "block";
	var initialHeight = 50;
	//obj.firstChild.innerHTML = "";
		var countTime = setInterval(function() { 
		initialHeight = initialHeight -2;
		obj.parentNode.parentNode.childNodes[2].firstChild.style.height = initialHeight.toString()+"px" ;
		if(initialHeight == 30)
		{
			clearInterval(countTime);
		}
		}, 50); 
	
	return false;
}

function fastAudit(aid,rid,isAudit,isPass)
{
	AIsAudit = isAudit;
	AIsPass = isPass;
	Aaid=aid;
	Arid=rid;
	passAudit();
}

function downloadFile(url){
    navigator.app.loadUrl(encodeURI(url), { openExternal:true}); 
}
    
