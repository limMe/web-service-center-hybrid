// This is for Search Panel

function getSearchResult(queryString)
{
	moveChildren("search_start");
    
	var url =domain+ "servcenter/Admin/Index/getlist?txt=" + queryString;
	if(queryString == "")
	{
		alert("查询内容不能为空");
		return 0;
	}
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",url);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			var searchJSON = JSON.parse(xmlhttp.responseText);
			if(searchJSON.code != 0)
				return 1;
			if(searchJSON.data.count == "0")
				alert("没有找到任何结果");
			for(var i=0;i<searchJSON.data.data.length;i++)
			{
				
				search_add(searchJSON.data.data[i].aid,searchJSON.data.data[i].rid,AIsAuditPanel,searchJSON.data.data[i].user.logo50,searchJSON.data.data[i].sname.substr(0,15),searchJSON.data.data[i].uname,searchJSON.data.data[i].ctime);
			}
		}
	}
	xmlhttp.send();
}

//忘了isaudit这个变量起什么作用了。。。
function search_add(aid,rid,isAudit,img,sname,uname,time)
{
	var jsSentence = "javascript: showDetail('"+aid+"','"+rid+"','"+isAudit+"')";
	var a = document.createElement("a");
	a.setAttribute("onClick",jsSentence);
	a.innerHTML = '<div><div><img src="'+img+'" ></div><div><p>'+sname+'</p><p>姓名: <em>'+uname+'</em>  时间:'+ time +' </p></div></div>';	
	document.getElementById("search_start").appendChild(a);
}

function searchKey(e) 
{
	var currKey=0,e=e||event; 
	currKey=e.keyCode||e.which||e.charCode;
	if (currKey == 13) 
	{
		getSearchResult(document.getElementById("SearchInput").value);
	}
} 