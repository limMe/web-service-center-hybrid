// JavaScript Document

function setSetting(uid) {
	var url = domain + "CrossDomainData/Notice/getUserInfo?uid=" + uid;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",url);
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			var settingJSON = JSON.parse(xmlhttp.responseText);
			var reg=new RegExp("\/","g");
			settingJSON.data.logo50 = settingJSON.data.logo50.replace(reg,"/");
			document.getElementById("userhead").src= settingJSON.data.logo50;
			document.getElementById("userinfo_name").innerHTML = decodeURI(settingJSON.data.real_name);
			document.getElementById("userinfo_uid").innerHTML = uid;
		}
	}
	xmlhttp.send();
}

function loginOff() {
	localStorage.clear();
	location.reload();
}