// 这个js文件方法适用于整个框架，不是具体功能的实现

//所有全局变量都到这里来。
var domain="http://hunan.tiup.cn/";
var uid="123456";

//AuditPage 需要这段全局变量传递参数
var Arid="";
var Aaid="";
var AIsAudit=0;
var AIsPass=0;
var AIsAuditPanel=1;

var LIsInitial=1;

var DFromSearch = 0;
var DFromDetail = 0;
var DFromScan = 0;
var DFromChart = 0;


function showPanel(panelName,idName)
{
	document.getElementById("panelName").innerHTML=panelName;
	var l=document.getElementsByClassName("panel").length;
	for(var i=0;i<l;i++)
	{
		document.getElementsByClassName("panel").item(i).style.display="none";
	}
	document.getElementById(idName).style.display="block";
	
	//好丑的并列条件语句
	if(idName == "ListPage")
	{
		auditPanelChange(AIsAuditPanel);
		switchBarIcon("barIconList");
		if(LIsInitial == 1)
			list_refresh();
		DFromDetail = 0;
		DFromSearch = 0;
		LIsInitial = 0;
		document.getElementById("refreshBtn").style.display="block";
	}
	else
	{
		document.getElementById("isAudit").style.display = "none";
		document.getElementById("refreshBtn").style.display="none";
		document.getElementById("panelName").style.display = "block";
	}
	
	if(idName == "SearchPage")
	{
		DFromSearch = 1;
		switchBarIcon("barIconSearch");
	}
	
	if(idName == "SettingPage")
	{
		switchBarIcon("barIconSetting");
	}
	
	if(idName == "ChartPage")
	{
		switchBarIcon("barIconCheck");
        getChartList();
	}
    
    if(idName == "ChartDisplayPage")
    {
        document.getElementById("backToHome").style.display = "block"; 
        DFromChart = 1;
    }
	
	if(idName == "ScanPage")
	{
		getBarcode();	
	}
}

function switchBarIcon(iconName)
{
	document.getElementById("barIconList").src = "img/TabBarIconList2.png";
	document.getElementById("barIconSearch").src = "img/TabBarIconSearch2.png";
	document.getElementById("barIconSetting").src = "img/TabBarIconSetting2.png";
	document.getElementById("barIconCheck").src = "img/TabBarIconCheck2.png";
	
	//这一句太hack风啦，不知道激活状态少个2的同志绝对读不懂
	var s = document.getElementById(iconName).src;
	document.getElementById(iconName).src = s.substr(0,s.length-5)+".png";
}

function backToHome()
{
	document.getElementById("backToHome").style.display = "none";
	var l=document.getElementsByClassName("panel").length;
	for(var i=0;i<l;i++)
	{
			document.getElementsByClassName("panel").item(i).style.display="none";
	}
	
	if(DFromSearch == 1)
	{
        //两行代码是不更新原有列表的
		document.getElementById("panelName").innerHTML="搜索";
		document.getElementById("SearchPage").style.display="block";  
	}	
    else if(DFromScan == 1)
	{
		showPanel("扫描","ScanPage");
	}
    else if(DFromChart ==1)
    {
        document.getElementById("panelName").innerHTML="统计";
		document.getElementById("ChartPage").style.display="block"; 
    }
    else
    {		
        auditPanelSwitch(AIsAuditPanel)		
		document.getElementById("panelName").innerHTML="表单";		
		document.getElementById("ListPage").style.display="block";
		document.getElementById("refreshBtn").style.display = "block";
    }
	document.getElementById("panelName").style.fontSize = "22px";
	document.getElementById("footer").style.display = "block";	
	document.getElementById("footer2").style.display = "none";
	
	DFromScan = 0;
	DFromDetail = 0;
	DFromSearch = 0;
    DFromChart =0;
}

function getProperLength(otherpart,fontsize){
    
    var maxWidth = document.body.clientWidth;
	var length = Math.floor( (maxWidth - otherpart) / fontsize );
	return length;
}

function getBarcode()
{
	var scanner = cordova.require("cordova/plugin/BarcodeScanner");
	scanner.scan(
	function (result) {	
		if(result.text != "")
		{
			var url =domain+ "servcenter/Admin/Index/getlist?txt=" + result.text;
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET",url,false);
			xmlhttp.onreadystatechange = function(){
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
					var searchJSON = JSON.parse(xmlhttp.responseText);
					if(searchJSON.code != 0)
					{
						alert("这不是有效的条码");
						return 1;
					}
				
					if(searchJSON.data.count == 0)
					{
						alert("没有找到任何结果");
						return 1;
					}
				
					if(searchJSON.code == 0 && searchJSON.data.count != 0)
					{
						var DFromScan = 1;
						showDetail(searchJSON.data.data[0].aid,searchJSON.data.data[0].rid,1);
					}
				}
			}
			xmlhttp.send();
		}
	},
	function (error) {
		alert("扫描失败");
	});
}

function moveChildren(parentID) {
    var div = document.getElementById(parentID);
    while(div.hasChildNodes()) 
    {
        div.removeChild(div.firstChild);
    }
}